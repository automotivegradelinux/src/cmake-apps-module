# Overview

The CMake AGL Framework Template Module
helps to build applications or bindings for the
AGL Application Framework.
You can use the template module to easily build a widget and its related
test widget for running on top of the AGL Application Framework.

This topic consists of the following:

- [Installing CMake Templates](installing-cmake.html).

- [Using CMake Templates](using-cmake.html).

- [Configuring CMake](configuring-cmake.html).

- [Project Architecture](project-architecture.html).

- [Advanced Usage](advanced-usage.html).

- [Advanced Customization](advanced-customization.html).

- [Autobuild](autobuild.html).


