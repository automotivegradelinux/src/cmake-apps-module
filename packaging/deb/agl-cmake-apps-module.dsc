Format: 3.0 (native)
Source: agl-cmake-apps-module
Binary: agl-cmake-apps-module-bin, agl-cmake-apps-module-dev
Architecture: any
Version: 6.90-0
Maintainer: Romain Forlot <romain.forlot@iot.bzh>
Standards-Version: 3.8.2
Homepage: https://gerrit.automotivelinux.org/gerrit/src/cmake-apps-module
Build-Depends: debhelper (>= 5),
 dpkg-dev,
 cmake,
DEBTRANSFORM-RELEASE: 1
Files:
 cmake-apps-module_6.90.tar.gz
